//
//  LocationViewController.swift
//  locationtracker
//
//  Created by Daniel Schulz on 24/01/2017.
//  Copyright © 2017 Daniel Schulz. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import RealmSwift

class LocationViewController: UIViewController {
  // MARK: - Outlets
  @IBOutlet var mapView: MKMapView!
  fileprivate var locations = [MKPointAnnotation]()
  public var lastUpdate: Date?;
  var lastLocation:CLLocation=CLLocation();
  var updatesEnabled: Bool = false;
  var backgrounded: Bool = false;
  var defferingUpdates:Bool = false;
  var desiredAccuracy:CLLocationAccuracy = kCLLocationAccuracyBest;
  
  public var routeLine: MKPolyline = MKPolyline() //your line
  public var routeLineView: MKPolylineView = MKPolylineView(); //overlay view
  
  var defaultCentre: NotificationCenter = NotificationCenter.default
  //- NSUserDefaults - LocationServicesControl_KEY to be set to TRUE when user has enabled location services.
  let UDefaults: UserDefaults = UserDefaults.standard;
  let LocationServicesControl_KEY: String = "LocationServices"
  
  public lazy var locationManager: CLLocationManager = {
    let manager = CLLocationManager()
    manager.distanceFilter = 20;
    manager.desiredAccuracy = kCLLocationAccuracyBest
    manager.delegate = self
    manager.requestAlwaysAuthorization()
    return manager
  }()
  
  public lazy var locationAPI: LocationAPI = {
    let api = LocationAPI()
    return api
  }()
  override func viewDidLoad() {
    defaultCentre.addObserver(self, selector: #selector(applicationWillResignActive), name: Notification.Name.UIApplicationWillResignActive, object: nil)
    defaultCentre.addObserver(self, selector: #selector(applicationWillEnterForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
  }
  
  func applicationWillResignActive(){
    if(self.updatesEnabled){
      backgrounded=true;
      locationManager.stopUpdatingLocation();
      locationManager.desiredAccuracy=desiredAccuracy;
      locationManager.distanceFilter = 50;
      locationManager.pausesLocationUpdatesAutomatically=false;
      locationManager.activityType = .fitness;
      locationManager.startUpdatingLocation();
    }
  }
  func applicationWillEnterForeground(){
    if(self.updatesEnabled){
      backgrounded=false;
      locationManager.stopUpdatingLocation();
      locationManager.desiredAccuracy=desiredAccuracy;
      locationManager.distanceFilter = 50;
      locationManager.pausesLocationUpdatesAutomatically=false;
      locationManager.activityType = .fitness;
      locationManager.startUpdatingLocation();
    }
  }
  
  
  // MARK: - Actions
  @available(iOS 9.0, *)
  @IBAction func enabledChanged(_ sender: UISwitch) {
    if sender.isOn {
      self.UDefaults.set(true, forKey: self.LocationServicesControl_KEY)
      locationManager.startUpdatingLocation()
      locationManager.allowsBackgroundLocationUpdates = true
      self.updatesEnabled = true;
    } else {
      self.UDefaults.set(false, forKey: self.LocationServicesControl_KEY)
      locationManager.stopUpdatingLocation()
      self.updatesEnabled = false;
      locationManager.allowsBackgroundLocationUpdates = false
    }
  }
  @IBAction func accuracyChanged(_ sender: UISegmentedControl) {
    let accuracyValues = [
      kCLLocationAccuracyBestForNavigation,
      kCLLocationAccuracyBest,
      kCLLocationAccuracyNearestTenMeters,
      kCLLocationAccuracyHundredMeters,
      kCLLocationAccuracyKilometer,
      kCLLocationAccuracyThreeKilometers]
    desiredAccuracy=accuracyValues[sender.selectedSegmentIndex];
    locationManager.desiredAccuracy = accuracyValues[sender.selectedSegmentIndex];
  }
  
}

// MARK: - MKMapViewDelegate
extension LocationViewController: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    if(overlay is MKPolyline)
    {
      let lineView = MKPolylineRenderer(overlay: overlay)
      lineView.strokeColor = UIColor.blue;
      lineView.alpha=0.5;
      return lineView;
    }
    return MKOverlayRenderer();
  }
  
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    if !annotation.isKind(of: MKCustomPointAnnotation.self){
      return nil;
    }
    let annotationView=MKPinAnnotationView.init(annotation: annotation, reuseIdentifier: "pin");
   
    let pinButton = UIButton.init(type:.roundedRect);
    pinButton.frame = CGRect.init(x: 0, y: 0, width: annotationView.frame.width, height: annotationView.frame.height);
    pinButton.tag=10;

    let tap = UITapGestureRecognizer.init(target: self, action: #selector(handlePinButtonTap))
    
    tap.numberOfTapsRequired = 1;
    
    pinButton.addGestureRecognizer(tap);
    
    annotationView.addSubview(pinButton);
    annotationView.canShowCallout=true;
    annotationView.rightCalloutAccessoryView = UIButton.init(type: .detailDisclosure);
    return annotationView;
  }
  func handlePinButtonTap(sender:UIView){
    print("tapped");
  }
  
  func centerMapOnLocation(location:Location){
    var mapRegion:MKCoordinateRegion=MKCoordinateRegion();
    let coords = CLLocationCoordinate2D.init(latitude: location.lat, longitude: location.lon);
    mapRegion.center = coords;
    mapRegion.span.latitudeDelta = 0.005;
    mapRegion.span.longitudeDelta = 0.005;
    self.mapView.setRegion(mapRegion, animated: true)
  }
  
  func addPositionToMapView(location:Location){
    let position=MKCustomPointAnnotation.init(location: location);
    mapView.addAnnotation(position);
  }
  func redrawMap(){
    let todaysLocations=LocationRealm.sharedInstance.getLocations(from: Date.init());
    for location in todaysLocations {
      addPositionToMapView(location: location);
    }
    addRoute();
    if let lastLocation=todaysLocations.last{
      centerMapOnLocation(location: lastLocation);
    }
  }
  
  func addRoute() {
    mapView.remove(self.routeLine);
    var pointsToUse: [CLLocationCoordinate2D] = [];
    for i in locations {
      pointsToUse += [i.coordinate];
    }
    self.routeLine = MKPolyline(coordinates: &pointsToUse, count: pointsToUse.count)
    mapView.add(self.routeLine)
  }

}


// MARK: - CLLocationManagerDelegate
extension LocationViewController: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    print("update Location");
    guard let mostRecentLocation = locations.last else {
      return
    }
    lastLocation=mostRecentLocation
    //tell the centralManager that you want to deferred this updatedLocation
    if (backgrounded && !defferingUpdates)
    {
      defferingUpdates = true;
      locationManager.allowDeferredLocationUpdates(untilTraveled: 50, timeout: 10);
    }else{
      handleLocation(location: lastLocation);
    }
  }
   
  func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
    defferingUpdates=false;
    handleLocation(location: lastLocation);
  }
  
  func handleLocation(location:CLLocation){
    self.locationAPI.postLocation(location: location)
    let realmLocation=LocationRealm.sharedInstance.addLocation(location: location);
    if let lastLocationUpdateTime=lastUpdate{
      let timeBetween=location.timestamp.timeIntervalSince(lastLocationUpdateTime);
      if(timeBetween.isLess(than: 60*5)){
        let realmPlace = LocationRealm.sharedInstance.addPlace(location: realmLocation);
      }
    }
    self.redrawMap();
    lastUpdate=location.timestamp;
  }
}



