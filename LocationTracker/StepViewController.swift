//
//  StepViewController.swift
//  SampleBit
//
//  Created by Ryan LaSante on 9/14/16.
//  Copyright © 2016 Fitbit. All rights reserved.
//

import UIKit

let SECONDS_PER_MINUTE = 60.0
let MINUTES_PER_HOUR = 60.0
let HOURS_PER_DAY = 24.0
let SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR
let SECONDS_PER_DAY = HOURS_PER_DAY * SECONDS_PER_HOUR

@available(iOS 9.0, *)
class StepViewController: UIViewController , AuthenticationProtocol {
  
  var authenticationController: AuthenticationController?;
  @IBOutlet var stepLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if(FitbitAPI.sharedInstance.session == nil){
      let authToken=UserDefaults.standard.string(forKey: "fitbit_token");
      guard authToken != nil else{
        self.authenticationController = AuthenticationController(delegate: self)
        self.authenticationController?.login(fromParentViewController: self)
        return;
      }
      FitbitAPI.sharedInstance.authorize(with: authToken!)
      refreshData();
      
    }
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    refreshData();
  }
  func authorizationDidFinish(_ success: Bool) {
    print("Hello World with \(success)!")
    guard let authToken = authenticationController?.authenticationToken else {
      return
    }
    UserDefaults.standard.set(authToken, forKey: "fitbit_token");
    FitbitAPI.sharedInstance.authorize(with: authToken)
    refreshData();
  }
  
  func refreshData(){
    let _ = StepStat.fetchTodaysStepStat() { [weak self] stepStat, error in
      let steps = stepStat?.steps ?? 0
      self?.stepLabel.text = "\(steps)"
    }

  }
  
}
