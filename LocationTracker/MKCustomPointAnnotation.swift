//
//  MKCustomPointAnnotation.swift
//  LocationTracker
//
//  Created by Daniel Schulz on 2/26/17.
//  Copyright © 2017 Razeware, LLC. All rights reserved.
//

import Foundation
import MapKit

class MKCustomPointAnnotation: MKPointAnnotation{
  var location:Location?=nil;
  
  init(location:Location) {
    super.init();
    self.coordinate=CLLocationCoordinate2D.init(latitude: location.lat, longitude: location.lon);
    self.location=location;
  }
}
