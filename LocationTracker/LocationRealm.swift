//
//  LocationRealm.swift
//  LocationTracker
//
//  Created by Daniel Schulz on 2/26/17.
//  Copyright © 2017 Razeware, LLC. All rights reserved.
//
import Foundation
import CoreLocation
import RealmSwift

class LocationRealm{
  
  static let sharedInstance: LocationRealm = LocationRealm();

  func addLocation(location:CLLocation) -> (Location){
    let realmLocation = Location(value: [
      "lat" : location.coordinate.latitude,
      "lon": location.coordinate.longitude,
      "speed": location.speed,
      "datetime": location.timestamp
      ]);
    let realm = try! Realm()
    try! realm.write {
      realm.add(realmLocation)
    }
    return realmLocation;
  }
  
  func addPlace(location:Location) -> (Place){
    let realmPlace = Place(value:[
      "location": location
      ]);
    let realm = try! Realm()
    try! realm.write {
      realm.add(realmPlace)
    }
    return realmPlace;
  }
  
  func getLocations(from:Date) -> Results<Location>{
    // Get the current calendar with local time zone
    var calendar = Calendar.current
    calendar.timeZone = TimeZone.current;
    
    // Get today's beginning & end
    let dateFrom = calendar.startOfDay(for: from) // eg. 2016-10-10 00:00:00
    var components = calendar.dateComponents([.year, .month, .day, .hour, .minute],from: dateFrom)
    components.day! += 1
    let dateTo = calendar.date(from: components)! // eg. 2016-10-11 00:00:00
    // Note: Times are printed in UTC. Depending on where you live it won't print 00:00:00 but it will work with UTC times which can be converted to local time
    
    // Set predicate as date being today's date
    let datePredicate = NSPredicate(format: "(%@ <= datetime) AND (datetime < %@)", argumentArray: [dateFrom, dateTo])
    
    let realm = try! Realm()
    let results=realm.objects(Location.self).filter(datePredicate).sorted(byKeyPath: "datetime", ascending: true);
    return results;
  }

}
