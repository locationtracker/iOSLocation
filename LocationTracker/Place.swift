//
//  Place.swift
//  LocationTracker
//
//  Created by Daniel Schulz on 2/26/17.
//  Copyright © 2017 Razeware, LLC. All rights reserved.
//

import Foundation
import RealmSwift

class Place:Object{
  dynamic var name="";
  dynamic var location:Location?
}
