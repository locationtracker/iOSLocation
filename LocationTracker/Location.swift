//
//  Location.swift
//  LocationTracker
//
//  Created by Daniel Schulz on 2/26/17.
//  Copyright © 2017 Razeware, LLC. All rights reserved.
//

import Foundation
import RealmSwift

class Location:Object{
  dynamic var lat = 0.0;
  dynamic var lon = 0.0;
  dynamic var datetime = Date(timeIntervalSince1970: 1);
  dynamic var speed = 0.0;
}
