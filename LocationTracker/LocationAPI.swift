//
//  LocationAPI.swift
//  locationtracker
//
//  Created by Daniel Schulz on 24/01/2017.
//  Copyright © 2017 Daniel Schulz. All rights reserved.
//

import Foundation
import CoreLocation

class LocationAPI: NSObject, URLSessionDelegate {
  
  static let sharedInstance: LocationAPI = LocationAPI()
  
  func postLocation(location: CLLocation){
    let myUrl = URL(string: "https://loc.schulz.codes/locations");
    var request = URLRequest(url:myUrl!)
    request.httpMethod="POST";
    let postString="lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&speed=\(location.speed)&timestamp=\(location.timestamp.timeIntervalSince1970)";
    request.httpBody = postString.data(using: String.Encoding.utf8);
    //print(location);
    let session = URLSession.init(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)

    let task = session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
      
      if error != nil
      {
        print("error=\(error)")
        return
      }
      print("successful Request");
      
      //Let's convert response sent from a server side script to a NSDictionary object:
      do {
        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
        if let parseJSON = json {
          // Now we can access value of First Name by its key
          _ = parseJSON["firstName"] as? String
        }
      } catch {
        print(error)
      }
    }
    task.resume();
    
  }
  
  func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
    completionHandler(.useCredential,URLCredential(trust: challenge.protectionSpace.serverTrust!));
  }
  
}
